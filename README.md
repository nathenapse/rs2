# Rs2

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="450"></p>

🔎 **Smart, Extensible Build Framework**

## Development

Since we used Nx Dev which is a mono repo with a singe package manager we only have a singe dependency

### Initial

- Run `npm install`

### Server

For the server I used docker for the simplicity of development. It will watch changed and auto reload

It is Built with Node Js Using nestJs as a framework. Uses simple repository pattern in the background

**Docker**

- Run `docker-compose up -d`
- This will start the server in port 3000
- You can navigate to `http://localhost:300/api` to find the api documentation

### Frontend

For the frontend used `VueJs` with `Vuex` and bare bone CSS which is not 100 percent finished

On the routing i have lazy loaded multiple routes and also have a layout

**NxDev**
Why NX

- It helps us manage a monorepo structure for our frontend
- For Example our Auth Module is found under the `Libs` folder. which will help us use our authentication module even in another application.

**Run IT**

- first copy the `.env.example` file located in the `apps/public/.env.expmple` and rename it to `.env`
- On the terminal run `nx start public`
- will start the application in `http://localhost:8080`

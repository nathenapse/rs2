import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import { actions } from './actions';
import { mutations } from './mutations';
import { getters } from './getters';
import { state } from './state';
import { AuthState } from '@rs2/frontend-shared';

Vue.use(Vuex);

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules: { AuthState },
  plugins: [
    createPersistedState({
      key: 'rs2-data',
      reducer: (state) => ({
        AuthState: state.AuthState,
      }),
    }),
  ],
});

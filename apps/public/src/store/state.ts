import { BasketModel } from '../models/Basket';
import { ProductModel } from '../models/Product';

export const state: { products: ProductModel[]; basket: BasketModel[] } = {
  products: [],
  basket: [],
};

export type State = typeof state;

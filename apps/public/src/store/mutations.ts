import { MutationTree } from 'vuex';
import { ProductModel } from '../models/Product';
import { State } from './state';

export enum MutationTypes {
  ADD_PRODUCT = 'ADD_PRODUCT',
  BASKETS = 'BASKETS',
  SEARCH_RESULTS = 'SEARCH_RESULTS',
  UPDATE_PRODUCT = 'UPDATE_PRODUCT',
  DELETE_PRODUCT = 'DELETE_PRODUCT',
}

export type Mutations<S = State> = {
  [MutationTypes.ADD_PRODUCT](state: S, data: ProductModel): void;
  [MutationTypes.BASKETS](state: S, data: ProductModel): void;
  [MutationTypes.SEARCH_RESULTS](state: S, data: ProductModel[]): void;
  [MutationTypes.UPDATE_PRODUCT](state: S, data: ProductModel): void;
  [MutationTypes.DELETE_PRODUCT](state: S, data: number): void;
};

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.ADD_PRODUCT](state, data: ProductModel): void {
    state.products.unshift(data);
  },
  [MutationTypes.SEARCH_RESULTS](state, data: ProductModel[]): void {
    state.products = data;
  },
  [MutationTypes.BASKETS](state, data: ProductModel): void {
    const index = state.basket.findIndex((item) => item.product.id == data.id);
    if (index === -1) {
      state.basket.push({ product: data, amount: 1 });
    } else {
      const basket = [...state.basket];
      const item = basket[index];
      item.amount = item.amount + 1;
      basket[index] = item;
      state.basket = [...basket];
    }
    state.products = [];
  },
  [MutationTypes.UPDATE_PRODUCT](state, data: ProductModel): void {
    const existsAtIndex = state.products.findIndex(
      (product) => product.id === data.id
    );
    if (existsAtIndex !== -1) {
      state.products[existsAtIndex] = data;
    }
  },
  [MutationTypes.DELETE_PRODUCT](state, id: number): void {
    state.products = state.products.filter((product) => product.id !== id);
  },
};

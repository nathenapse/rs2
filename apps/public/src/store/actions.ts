import { ActionContext, ActionTree } from 'vuex';
import {
  addProductToBasket,
  getUserBasket,
  searchProductList,
} from '../features/product/api/product.api';
import { ProductModel, ProductType } from '../models/Product';
import { Mutations, MutationTypes } from './mutations';
import { State } from './state';

export enum ActionTypes {
  ADD_PRODUCT = 'ADD_PRODUCT',
  GET_BASKET = 'GET_BASKET',
  SEARCH_PRODUCT = 'SEARCH_PRODUCT',
  UPDATE_PRODUCT = 'UPDATE_PRODUCT',
  DELETE_PRODUCT = 'DELETE_PRODUCT',
}

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, State>, 'commit'>;

export interface Actions {
  [ActionTypes.SEARCH_PRODUCT](
    { commit }: AugmentedActionContext,
    payload: { text: string; type?: ProductType }
  ): void;
  [ActionTypes.GET_BASKET](
    { commit }: AugmentedActionContext,
    payload: null
  ): void;
  [ActionTypes.ADD_PRODUCT](
    { commit }: AugmentedActionContext,
    payload: ProductModel
  ): void;
  [ActionTypes.UPDATE_PRODUCT](
    { commit }: AugmentedActionContext,
    payload: ProductModel
  ): void;
  [ActionTypes.DELETE_PRODUCT](
    { commit }: AugmentedActionContext,
    payload: ProductModel
  ): void;
}

export const actions: ActionTree<State, any> & Actions = {
  [ActionTypes.SEARCH_PRODUCT](
    { commit },
    payload: { text: string; type?: ProductType }
  ) {
    searchProductList(payload.text, payload.type).then((result) => {
      commit(MutationTypes.SEARCH_RESULTS, result);
    });
  },
  [ActionTypes.GET_BASKET]({ commit }, payload: null) {
    getUserBasket().then((result) => {
      commit(MutationTypes.BASKETS, result);
    });
  },
  [ActionTypes.ADD_PRODUCT]({ commit }, payload: ProductModel) {
    commit(MutationTypes.BASKETS, payload);
  },
  [ActionTypes.UPDATE_PRODUCT]({ commit }, payload: ProductModel) {
    const data: ProductModel = { ...payload };
    commit(MutationTypes.UPDATE_PRODUCT, data);
  },
  [ActionTypes.DELETE_PRODUCT]({ commit }, payload: ProductModel) {
    commit(MutationTypes.DELETE_PRODUCT, payload.id);
  },
};

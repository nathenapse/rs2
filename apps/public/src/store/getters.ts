import { GetterTree } from 'vuex';
import { ProductModel } from '../models/Product';
import { State } from './state';

export type Getters = {
  products(state: State): ProductModel[];
  getProduct(state: State): (id: number) => ProductModel;
};

export const getters: GetterTree<State, any> & Getters = {
  products: (state) => {
    return state.products;
  },
  basket: (state) => {
    return state.basket;
  },
  totalItems: (state) => {
    return state.basket.reduce((reduced, item) => reduced + item.amount, 0);
  },
  getProduct: (state) => (id: number) => {
    return state.products.find((product) => product.id === id);
  },
};

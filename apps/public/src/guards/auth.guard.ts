import store from '../store';

export const NonAuth = (to, from, next) => {
  if (!store.getters['AuthState/token']) {
    next();
    return;
  }
  next('/');
};

export const Auth = (to, from, next) => {
  if (!store.getters['AuthState/token']) {
    next('/auth/login');
    return;
  }

  next();
};

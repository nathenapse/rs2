import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import ProductList from '../features/product/views/ProductList.vue';
import ProductSearch from '../features/product/views/ProductSearch.vue';
import { Auth, NonAuth } from '../guards/auth.guard';
import DefaultLayout from '../layouts/DefaultLayout.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: DefaultLayout,
    children: [
      {
        path: '',
        name: 'Products',
        component: ProductList,
      },
      {
        path: 'search',
        name: 'ProductSearch',
        component: ProductSearch,
      },
    ],
    beforeEnter: Auth,
  },
  {
    path: '/auth/login',
    name: 'Login',
    component: () =>
      import(/* webpackChunkName: "login" */ '@rs2/frontend-shared').then(
        ({ LoginView }) => LoginView
      ),
    beforeEnter: NonAuth,
  },
  {
    path: '*',
    component: () =>
      import(/* webpackChunkName: 'NotFoundView' */ '../views/404.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;

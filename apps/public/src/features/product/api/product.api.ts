import { ProductEndpoints } from './product.endpoints';
import { http } from '@rs2/frontend-shared';
import { ProductModel, ProductType } from 'apps/public/src/models/Product';

export const getProductList = () => {
  return http.get(ProductEndpoints.getAllProducts).then((response) => {
    return response.data;
  });
};
export const searchProductList = (
  searchTerm: string,
  category: ProductType
) => {
  const params = [];
  if (searchTerm) params.push(`"name":{"$contL":"${searchTerm}"}`);
  if (category) params.push(`"type":{"$eq":"${category}"}`);
  const param = `{${params.join(',')}}`;
  return http
    .get(ProductEndpoints.getAllProducts, {
      params: { s: param },
    })
    .then((response) => {
      return response.data;
    });
};
export const getUserBasket = () => {
  return http
    .get(ProductEndpoints.getBaskets, {
      params: { join: `product` },
    })
    .then((response) => {
      return response.data;
    });
};

export const addProductToBasket = (product: ProductModel) => {
  return http
    .post(ProductEndpoints.getBaskets, { id: product.id })
    .then((response) => {
      return response.data;
    });
};

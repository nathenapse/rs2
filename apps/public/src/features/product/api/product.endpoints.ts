export const ProductEndpoints = {
  getAllProducts: `/product`,
  searchAllProducts: `/product`,
  getBaskets: `/basket`,
};

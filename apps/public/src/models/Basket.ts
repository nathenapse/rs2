import { ProductModel } from './Product';

export interface BasketModel {
  amount: number;
  product: ProductModel;
}

export interface ProductModel {
  id: number;
  name: string;
  type: ProductType;
}

export enum ProductType {
  Books = 'Books',
  Music = 'Music',
  Games = 'Games',
}

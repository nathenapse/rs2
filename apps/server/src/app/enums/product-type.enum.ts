export enum ProductTypes {
  Books = 'Books',
  Music = 'Music',
  Games = 'Games',
}

import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class AddProductDto {
  @ApiProperty({ required: true, maxLength: 20 })
  @IsNumber()
  public id: number;
}

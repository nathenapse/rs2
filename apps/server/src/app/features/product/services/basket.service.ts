import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BasketEntity } from '../../../entities/basket.entity';
@Injectable()
export class BasketService extends TypeOrmCrudService<BasketEntity> {
  constructor(@InjectRepository(BasketEntity) repo) {
    super(repo);
  }
}

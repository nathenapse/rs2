import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BasketEntity } from '../../entities/basket.entity';
import { ProductEntity } from '../../entities/product.entity';
import { BasketController } from './controllers/basket.controller';
import { ProductController } from './controllers/product.controller';
import { BasketService } from './services/basket.service';
import { ProductService } from './services/product.service';

@Module({
  imports: [TypeOrmModule.forFeature([BasketEntity, ProductEntity])],
  controllers: [ProductController, BasketController],
  providers: [BasketService, ProductService],
})
export class ProductModule {}

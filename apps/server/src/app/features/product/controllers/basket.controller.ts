import {
  Controller,
  NotImplementedException,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  CrudRequest,
  CrudRequestInterceptor,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { IsNumber } from 'class-validator';
import { BasketEntity } from '../../../entities';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { AddProductDto } from '../dtos/addProduct.dto';
import { BasketService } from '../services/basket.service';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@Crud({
  model: {
    type: BasketEntity,
  },
})
@ApiTags('Basket')
@Controller('basket')
export class BasketController implements CrudController<BasketEntity> {
  constructor(public service: BasketService) {}

  get base(): CrudController<BasketEntity> {
    return this;
  }
}

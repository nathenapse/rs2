import { Controller, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  CreateManyDto,
  Crud,
  CrudController,
  CrudRequest,
  GetManyDefaultResponse,
} from '@nestjsx/crud';
import { ProductEntity } from '../../../entities/product.entity';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { ProductService } from '../services/product.service';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@Crud({
  model: {
    type: ProductEntity,
  },
})
@ApiTags('Product')
@Controller('product')
export class ProductController implements CrudController<ProductEntity> {
  constructor(public service: ProductService) {}

  get base(): CrudController<ProductEntity> {
    return this;
  }
}

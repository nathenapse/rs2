import { ApiProperty } from '@nestjs/swagger';

export class AuthDto {
  @ApiProperty({ required: true, maxLength: 20 })
  public username: string;

  @ApiProperty({ required: true, minLength: 4, maxLength: 50 })
  public password: string;
}

import { Body, Controller, Inject, Post, ValidationPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud } from '@nestjsx/crud';
import { UserEntity } from '../../../entities';
import { AuthDto } from '../dtos/auth.dto';
import { AuthService } from '../services/auth.service';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    @Inject(AuthService)
    private readonly authService: AuthService
  ) {}

  @Post('/register')
  public async register(@Body(ValidationPipe) credentials: AuthDto) {
    return this.authService.register(credentials);
  }

  @Post('/login')
  public async login(@Body(ValidationPipe) credentials: AuthDto) {
    return this.authService.login(credentials);
  }
}

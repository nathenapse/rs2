import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { sign } from 'jsonwebtoken';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { AuthDto } from '../dtos/auth.dto';
import { UserEntity } from './../../../entities/user.entity';

@Injectable()
export class AuthService extends TypeOrmCrudService<UserEntity> {
  constructor(@InjectRepository(UserEntity) repo) {
    super(repo);
  }
  async register(credentials: AuthDto) {
    try {
      credentials.username = credentials.username?.toLowerCase();
      const userExist = await this.repo.findOne({
        where: { username: credentials.username },
      });

      if (userExist == null) {
        const user = this.repo.create(credentials);
        await user.save();
        return user;
      } else {
        throw new ConflictException('Email has already been registered');
      }
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  createAccessToken(user: UserEntity) {
    const expiresIn = 604800;

    const accessToken = jwt.sign(
      {
        id: user.id,
        email: user.username,
      },
      process.env.SECRET_KEY,
      { expiresIn }
    );

    return {
      expiresIn,
      accessToken,
    };
  }

  async login({ username, password }: AuthDto) {
    const user = await this.repo.findOne({ username: username?.toLowerCase() });
    if (user != null) {
      const isValid = await user.comparePassword(password);
      const accessToken = this.createAccessToken(user);
      return { user: user, accessToken };
    } else {
      throw new BadRequestException('Invalid username or password');
    }
  }
}

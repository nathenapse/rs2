import { ApiProperty } from '@nestjs/swagger';
import { CrudValidationGroups } from '@nestjsx/crud';
import {
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';
import { Column, Entity, ManyToMany, OneToMany } from 'typeorm';
import { ProductTypes } from '../enums/product-type.enum';
import { AbstractEntity } from './../../shared/entities/abstract.entity';
import { BasketEntity } from './basket.entity';

const { CREATE, UPDATE } = CrudValidationGroups;
@Entity('product')
export class ProductEntity extends AbstractEntity {
  @ApiProperty({ required: true })
  @IsNotEmpty({ always: true })
  @IsString({ always: true })
  @MaxLength(32, { always: true })
  @Column({
    length: 32,
  })
  name: string;

  @ApiProperty({ required: true, enum: ProductTypes })
  @IsNotEmpty({ always: true })
  @IsEnum(ProductTypes, { always: true })
  @MaxLength(32, { always: true })
  @Column({ enum: ProductTypes, length: 8 })
  type: string;

  @ApiProperty({ required: true })
  @IsNotEmpty({ always: true })
  @IsString({ always: true })
  @MaxLength(100, { always: true })
  @Column({
    length: 100,
  })
  description: string;

  @ManyToMany(() => BasketEntity)
  baskets: BasketEntity[];
}

import { BeforeInsert, Column, Entity, OneToMany } from 'typeorm';
import { AbstractEntity } from '../../shared/entities/abstract.entity';
import * as bcrypt from 'bcrypt';
import { BasketEntity } from './basket.entity';
import { AuthDto } from '../features/auth/dtos/auth.dto';
import { Exclude } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

@Entity('user')
export class UserEntity extends AbstractEntity {
  @Column({
    length: 20,
  })
  username: string;
  @ApiProperty()
  @Exclude()
  @Column({
    length: 256,
  })
  password: string;

  @OneToMany(() => BasketEntity, (basket) => basket.user)
  baskets: BasketEntity[];

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  async comparePassword(attempt: string) {
    return bcrypt.compare(attempt, this.password);
  }

  fromDto(registerDto: AuthDto): UserEntity {
    this.username = registerDto.username;
    this.password = registerDto.password;
    return this;
  }
}

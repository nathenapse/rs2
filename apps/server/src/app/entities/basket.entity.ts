import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, Min } from 'class-validator';
import { Column, Entity, ManyToMany, ManyToOne } from 'typeorm';
import { AbstractEntity } from './../../shared/entities/abstract.entity';
import { ProductEntity } from './product.entity';
import { UserEntity } from './user.entity';

@Entity('basket')
export class BasketEntity extends AbstractEntity {
  @ApiProperty()
  @IsNotEmpty({ always: true })
  @Min(1, { always: true })
  @IsNumber()
  @Column()
  amount: number;

  @ApiProperty({ type: ProductEntity })
  @ManyToMany(() => ProductEntity)
  products: ProductEntity[];

  @ManyToOne(() => UserEntity)
  user: UserEntity;
}

module.exports = {
  projects: [
    '<rootDir>/apps/server',
    '<rootDir>/apps/public',
    '<rootDir>/libs/frontend-shared',
  ],
};

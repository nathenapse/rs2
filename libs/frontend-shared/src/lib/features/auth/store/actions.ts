import { ActionContext, ActionTree } from 'vuex';
import { login } from '../api/auth.api';
import { AuthResponse } from '../models/AuthResponse';
import { Mutations, MutationTypes } from './mutations';
import { State } from './state';

export enum ActionTypes {
  LOGIN = 'LOGIN',
  LOGOUT = 'LOGOUT',
}

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, State>, 'commit'>;

export interface Actions {
  [ActionTypes.LOGIN](
    { commit }: AugmentedActionContext,
    payload: { username: string; password: string }
  ): void;
  [ActionTypes.LOGOUT]({ commit }: AugmentedActionContext): void;
}

export const actions: ActionTree<State, State> & Actions = {
  [ActionTypes.LOGIN](
    { commit },
    payload: { username: string; password: string }
  ) {
    return login(payload.username, payload.password).then(
      (response: AuthResponse) => {
        commit(MutationTypes.LOGIN, response);
      }
    );
  },
  [ActionTypes.LOGOUT]({ commit }) {
    commit(MutationTypes.LOGOUT, null);
  },
};

import { MutationTree } from 'vuex';
import { AuthResponse } from '../models/AuthResponse';
import { State } from './state';

export enum MutationTypes {
  LOGIN = 'LOGIN',
  LOGOUT = 'LOGOUT',
}

export type Mutations<S = State> = {
  [MutationTypes.LOGIN](state: S, data: AuthResponse): void;
  [MutationTypes.LOGOUT](state: S, data?: null): void;
};

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.LOGIN](state, data: AuthResponse): void {
    state.token = data.accessToken.accessToken;
    state.user = data.user;
  },
  [MutationTypes.LOGOUT](state, data?: null): void {
    state.token = null;
    state.user = null;
  },
};

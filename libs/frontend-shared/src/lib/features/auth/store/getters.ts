import { GetterTree } from 'vuex';
import { User } from '../models/AuthResponse';
import { State } from './state';

export type Getters = {
  token(state: State): string;
  currentUser(state: State): User;
};

export const getters: GetterTree<State, State> & Getters = {
  token: (state) => {
    return state.token;
  },
  currentUser: (state) => {
    return state.user;
  },
};

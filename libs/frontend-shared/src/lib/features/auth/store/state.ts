import { User } from '../models/AuthResponse';

export interface AuthStateInterface {
  token: string;
  user: User;
}

export const state: AuthStateInterface = {
  token: null,
  user: null,
};

export type State = typeof state;

export const AuthEndpoints = {
  login: `/auth/login`,
  register: `/auth/register`,
};

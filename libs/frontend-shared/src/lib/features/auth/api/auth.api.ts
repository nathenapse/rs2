import http from '../../../services/http';
import { AuthEndpoints } from './auth.endpoints';

export const login = (username: string, password: string) => {
  return http
    .post(AuthEndpoints.login, { username, password })
    .then((response) => {
      return response.data;
    });
};

export const register = () => {
  return http.get(AuthEndpoints.login).then((response) => {
    return response.data;
  });
};

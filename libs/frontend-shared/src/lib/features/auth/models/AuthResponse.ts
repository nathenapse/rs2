export interface User {
  id: number;
  created: Date;
  updated: Date;
  deleted?: Date;
  username: string;
}

export interface AccessToken {
  expiresIn: number;
  accessToken: string;
}

export interface AuthResponse {
  user: User;
  accessToken: AccessToken;
}

import axios from 'axios';

const http = axios.create({
  baseURL: process.env.VUE_APP_API_URL + '/api/',
  headers: {},
});

try {
  http.interceptors.request.use(
    (config) => {
      try {
        const data = JSON.parse(localStorage.getItem('rs2-data'));
        if (data && data.AuthState.token) {
          config.headers['Authorization'] = `Bearer ${data.AuthState.token}`;
        }
        return config;
      } catch (error) {
        console.log(
          'Something bad happened while trying to parse the localStorage!'
        );
      }
    },
    (error) => {
      return Promise.reject(error);
    }
  );
} catch (error) {
  console.log(error);
}

export default http;
